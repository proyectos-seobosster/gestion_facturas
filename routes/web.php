<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\FacturaController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ProductoController;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/register', [AuthController::class, 'register'])->name('register');
Route::post('/register', [AuthController::class, 'create_register'])->name('create_register');
Route::get('/login', [AuthController::class, 'index']);
Route::post('/login', [AuthController::class, 'login'])->name('login');

Route::get('/', function () {
    return view('welcome');
});

Route::middleware('auth:sanctum')->group(function(){
    //DASHBOARD
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
    Route::post('/dashboard', [DashboardController::class, 'comprar'])->name('dashboard_compar');
    
    Route::middleware('administrador')->group(function(){
        //FACTURAR
        Route::get('/facturar', [FacturaController::class, 'index'])->name('facturar');
        Route::post('/facturar', [FacturaController::class, 'crear_factura'])->name('facturar_crear');
        Route::get('/ver_detalle', [FacturaController::class, 'ver_detalle'])->name('ver_detalle');

        //PRODUCTOS
        Route::get('/productos', [ ProductoController::class, 'index' ])->name('productos');        
        Route::get('/productos/new', [ ProductoController::class, 'create' ])->name('productos_new');
        Route::post('/productos/store', [ ProductoController::class, 'store' ])->name('productos_store');
        Route::get('/productos/show/{producto}', [ ProductoController::class, 'show' ])->name('productos_show');
        Route::put('/productos/update/{producto}', [ ProductoController::class, 'update' ])->name('productos_update');
        Route::post('/productos/delete/{producto}', [ ProductoController::class, 'destroy' ])->name('productos_destroy');
    });


    Route::post('/logout', [AuthController::class, 'logout'])->name('logout');

});