<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Compra;
use App\Models\Factura;
use Illuminate\Http\Request;

class FacturaController extends Controller
{
    public function index(){
        $facturas = Factura::get();
        $clientes = User::join('compras', 'compras.user_id', 'users.id')
            ->where('factura_id', null)
            ->select('users.id', 'users.name')->get();
        return view('facturas', compact('facturas', 'clientes'));
    }

    public function crear_factura(Request $request){
        $data = $request->validate([
            'cliente_id' => ['required'],
        ]);
        $factura = Factura::create();
        $compras = Compra::where('user_id', $data['cliente_id'])
            ->where('factura_id', null)->get();
        foreach($compras as $compra){
            $compra['factura_id'] = $factura->id;
            $compra->save();
            $compra = Compra::where('id', $compra->id)->with(['producto]' => function($query){
                return $query->with('impuesto');
            }]);
        }
        
        return view('detalle_factura', ['factura' => $factura, 'compras' => $compras]);
    }

    public function ver_detalle(Request $request){
        $data = $request->validate([
            'factura_id' => ['required'],
        ]);
        $factura = Factura::where('id', $data['factura_id'])
            ->with(['compras' => function($query){
                return $query->with(['producto' => function($query){
                    return $query->with('impuesto');
                }]);
            }])->first();
        return view('ver_detalle', ['factura' => $factura]);
    }
}
