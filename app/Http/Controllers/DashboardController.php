<?php

namespace App\Http\Controllers;

use App\Models\Compra;
use App\Models\Producto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index(){
        $productos = Producto::get();
        return view('dashboard', compact('productos'));
    }
    public function comprar(Request $request){
        $user_id = Auth::user()->id;
        $compra = Compra::create([
            'user_id' => $user_id,
            'producto_id' => $request->producto_id,
        ]);
        return redirect()->route('dashboard');
    }
}
