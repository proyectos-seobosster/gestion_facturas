<?php

namespace App\Http\Controllers;

use App\Models\Producto;
use Illuminate\Http\Request;

class ProductoController extends Controller
{
    public function index(){
        $productos = Producto::all();
        return view('productos.index', compact('productos'));
    }

    public function create(){
        return view('productos.new');
    }

    public function store(Request $request){
        $data = $request->validate([
            'name' => 'required',
            'description' => ['string', 'nullable'],
        ]);
        $producto = Producto::create($data);
        return redirect()->route('productos');
    }

    public function show(Producto $producto){
        return view('productos.edit', compact('producto'));
    }

    public function update(Request $request, Producto $producto){
        $data = $request->validate([
            'name' => ['string', 'nullable'],
            'description' => ['string', 'nullable'],
        ]);
        if($data['name']){
            $producto['name'] = $data['name'];
        }
        if($data['description']){
            $producto['description'] = $data['description'];
        }
        if($producto->isClean()){
            return back()->withInputs();
        }
        $producto->save();
        return redirect()->route('productos');
    }

    public function destroy(Producto $producto){
        $producto->delete();
        return redirect()->route('productos');
    }
}
