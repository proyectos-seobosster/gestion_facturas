<?php

namespace App\Models;

use App\Models\Producto;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Compra extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'producto_id',
    ];

    public function producto(){
        return $this->belongsTo(Producto::class);
    }

    public function cliente(){
        return $this->belongsTo(User::class);
    }

}
