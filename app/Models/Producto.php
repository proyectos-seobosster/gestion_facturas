<?php

namespace App\Models;

use App\Models\Impuesto;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Producto extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
    ];

    public function impuesto(){
        return $this->hasOne(Impuesto::class);
    }

    public function compras(){
        return $this->hasMany(Compra::class);
    }
}
