<?php

namespace Database\Seeders;

use App\Models\Impuesto;
use App\Models\Producto;
use Illuminate\Database\Seeder;

class ProductosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Producto::factory(1)->create([
            'name' => 'Producto 1',
            'description' => 'Description Producto 1'
        ])->each(function($producto){
            Impuesto::create([
                'producto_id' => $producto->id,
                'precio' => 123.45,
                'impuesto' => 5
            ]);
        });
        Producto::factory(1)->create([
            'name' => 'Producto 2',
            'description' => 'Description Producto 2'
        ])->each(function($producto){
            Impuesto::create([
                'producto_id' => $producto->id,
                'precio' => 45.65,
                'impuesto' => 15
            ]);
        });
        Producto::factory(1)->create([
            'name' => 'Producto 3',
            'description' => 'Description Producto 3'
        ])->each(function($producto){
            Impuesto::create([
                'producto_id' => $producto->id,
                'precio' => 39.73,
                'impuesto' => 12
            ]);
        });
        Producto::factory(1)->create([
            'name' => 'Producto 4',
            'description' => 'Description Producto 4'
        ])->each(function($producto){
            Impuesto::create([
                'producto_id' => $producto->id,
                'precio' => 250.00,
                'impuesto' => 8
            ]);
        });
        
        Producto::factory(1)->create([
            'name' => 'Producto 5',
            'description' => 'Description Producto 5'
        ])->each(function($producto){
            $impuesto = Impuesto::create([
                'producto_id' => $producto->id,
                'precio' => 59.35,
                'impuesto' => 10
            ]);
        });
    }
}
