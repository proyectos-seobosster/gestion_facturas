<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        $role_administrador = Role::create(['name' => 'administrador']);
        \App\Models\User::factory(1)->create([
            'name' => 'Administrador',
            'email' => 'administrador@correo.com',
            'password' => bcrypt('password'),
        ])->each(function($user) use($role_administrador){
            $user->assignRole($role_administrador);
        });
        $role_cliente = Role::create(['name' => 'cliente']);
        \App\Models\User::factory(1)->create([
            'name' => 'Cliente1',
            'email' => 'cliente1@correo.com',
            'password' => bcrypt('password'),
        ])->each(function($user) use($role_cliente){
            $user->assignRole($role_cliente);
        });
        \App\Models\User::factory(1)->create([
            'name' => 'Cliente2',
            'email' => 'cliente2@correo.com',
            'password' => bcrypt('password'),
        ])->each(function($user) use($role_cliente){
            $user->assignRole($role_cliente);
        });
        $this->call([
            ProductosSeeder::class,
        ]);
    }
}
