<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
    <a href="/">Inicio</a>
    <form class="form-control p-5" action="{{ route('create_register') }}" method="post">
        @csrf
        <div class="row">
            <div class="col-6 text-primary text-center h1">
                Registrarse
            </div>
        </div>
        <div class="row">
            <div class="col-6 text-primary text-center ">
                <input name="name" value="" class="form-control" type="text" placeholder="Nombre">
            </div>
        </div>
        <div class="row">
            <div class="col-6 text-primary text-center ">
                <input name="email" value="" class="form-control mt-3" type="text" placeholder="Correo Electrónico">
            </div>
        </div>
        <div class="row">
            <div class="col-6 text-primary text-center mt-3">
                <input name='password' class="form-control" type="password" placeholder="Contraseña">
            </div>
        </div>
        <div class="row">
            <div class="col-6 text-primary text-center mt-3">
                <input name='password_confirmation' class="form-control" type="password" placeholder="Confirmar contraseña">
            </div>
        </div>
        <button type="submit" class="btn btn-primary mt-3">Entrar</button>
    </form>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>