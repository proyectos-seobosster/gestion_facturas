@extends('layouts.app')
 
@section('title', 'Dashboard')
 
@section('content')
    @if(Illuminate\Support\Facades\Auth::user()->hasRole('administrador'))
        <form action="facturar" method="get">
            <button class="btn btn-primary" type="submit" >Facturar</button>
        </form>
    @endif
    <div class="h2 text-primary">
        Productos
    </div>
    <form action="{{ route('productos_store') }}" method="post">
        @csrf
        <div class="mb-3">
            <label for="exampleFormControlInput1" class="form-label">Nombre</label>
            <input type="text" class="form-control" id="name" name="name" placeholder="Nombre">
        </div>
        <div class="mb-3">
            <label for="exampleFormControlInput1" class="form-label">Descripción</label>
            <input type="text" class="form-control" id="description" name="description" placeholder="Descriptión">
        </div>
        <button class="btn btn-primary mb-5" type="submit">Crear</button>
    </form>

    <a href="/productos" class="btn btn-primary">Volver</a>

@endsection