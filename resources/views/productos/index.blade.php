@extends('layouts.app')
 
@section('title', 'Dashboard')
 
@section('content')
    @if(Illuminate\Support\Facades\Auth::user()->hasRole('administrador'))
        <form action="facturar" method="get">
            <button class="btn btn-primary" type="submit" >Facturar</button>
        </form>
    @endif
    <div class="h2 text-primary">
        Productos
        <a href="{{ route('productos_new') }}" class="btn btn-primary">Crear</a>
    </div>
    <table class="table">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Description</th>
                <th>Acción</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($productos as $producto)
                <tr>
                    <td>
                        {{ $producto->name }}
                    </td>
                    <td>
                        {{ $producto->description }}
                    </td>
                    <td>
                        <form action="{{ route('productos_show', $producto->id) }}" method="get">
                            <button class="btn btn-link text-success" type="submit">Editar</button>
                        </form>
                        <form action="{{ route('productos_destroy', $producto->id) }}" method="post">
                            @csrf
                            <button class="btn btn-link text-danger" type="submit">Eliminar</button>
                        </form>
                    </td>
                </tr>                
            @endforeach
        </tbody>
    </table>
    <a href="/dashboard" class="btn btn-primary">Volver</a>

@endsection