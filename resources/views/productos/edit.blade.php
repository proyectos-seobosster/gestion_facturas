@extends('layouts.app')
 
@section('title', 'Dashboard')
 
@section('content')
    @if(Illuminate\Support\Facades\Auth::user()->hasRole('administrador'))
        <form action="facturar" method="get">
            <button class="btn btn-primary" type="submit" >Facturar</button>
        </form>
    @endif
    <div class="h2 text-primary">
        Productos
    </div>
    <form action="{{ route('productos_update', $producto->id) }}" method="post">
        @csrf
        @method('put')
        <div class="mb-3">
            <label for="exampleFormControlInput1" class="form-label">Nombre</label>
            <input type="text" class="form-control" id="name" name="name" placeholder="Nombre" value="{{ old('name', $producto->name) }}">
        </div>
        <div class="mb-3">
            <label for="exampleFormControlInput1" class="form-label">Descripción</label>
            <input type="text" class="form-control" id="description" name="description" placeholder="Descriptión" value="{{ old('description', $producto->description) }}">
        </div>
        <button class="btn btn-success mb-5" type="submit">Actualizar</button>
    </form>

    <a href="/productos" class="btn btn-primary">Volver</a>

@endsection