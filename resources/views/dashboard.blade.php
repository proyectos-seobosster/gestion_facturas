@extends('layouts.app')
 
@section('title', 'Dashboard')
 
@section('content')
    @if(Illuminate\Support\Facades\Auth::user()->hasRole('administrador'))
    <div class="inline">
            <a class="btn btn-success" href="/productos" >Productos</a>
            <a class="btn btn-primary" href="/facturar" >Facturar</a>
    </div>
    @endif
        <div class="row">
            @foreach ($productos as $producto)        
                <div class="card col-3 m-3" style="width: 18rem;">
                    <div class="card-body">
                        <h5 class="card-title">{{ $producto->name }}</h5>
                        <p class="card-text">{{ $producto->description }}</p>
                        <form action="/dashboard" method="post">
                            @csrf
                            <input type="hidden" name="producto_id" value="{{ $producto->id }}">
                            <button type="submit" class="btn btn-primary">Comprar</button>
                        </form>
                    </div>
                </div>
            @endforeach
        </div>
@endsection