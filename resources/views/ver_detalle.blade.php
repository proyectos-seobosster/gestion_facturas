@extends('layouts.app')
 
@section('title', 'Dashboard')
 
@section('content')
    
    Factura Nro: {{ $factura->id }}
    <br>


    <table class="table">
        <thead>
            <tr>
                <th>Nombre</th>
                <td>Precio</td>
                <td>Impuesto</td>
                <td>Total</td>
            </tr>
        </thead>
        <tbody>
            @php
                $subtotal = 0;
                $total_impuesto=0;
                $total = 0;
            @endphp
            @foreach ($factura->compras as $compra)
                <tr>
                    <td>
                        {{ $compra->producto->name }}
                    </td>
                    <td>
                        {{ $precio = $compra->producto->impuesto->precio }}
                    </td>
                    <td>
                        {{ $impuesto = $compra->producto->impuesto->impuesto/100 * $compra->producto->impuesto->precio }}
                        &nbsp;&nbsp;|&nbsp;&nbsp;{{ $compra->producto->impuesto->impuesto }}%
                    </td>
                    <td>
                        {{ $compra->producto->impuesto->precio+$impuesto }}
                    </td>
                    @php
                        $subtotal += $precio;
                        $total_impuesto += $impuesto;
                        $total += $compra->producto->impuesto->precio+$impuesto;
                    @endphp
                </tr>
            @endforeach
            <tr>
                <td colspan="4">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">Sub Total</td>
                <td>{{ $subtotal }}</td>
            </tr>
            <tr>
                <td colspan="3">Total Impuesto</td>
                <td>{{ $total_impuesto }}</td>
            </tr>
            <tr>
                <td colspan="3">Total General</td>
                <td>{{ $total }}</td>
            </tr>
        </tbody>
    </table>
    <a href="/facturar" class="btn btn-primary">Volver</a>

@endsection