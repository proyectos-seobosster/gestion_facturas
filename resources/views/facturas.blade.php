@extends('layouts.app')
 
@section('title', 'Dashboard')
 
@section('content')
    <div class="h2 text-primary mt-5">Facturar</div>
    @php
        $cli=0;
    @endphp
    @foreach ($clientes as $cliente)
        @if($cli != $cliente->id)
            @php $cli=$cliente->id @endphp
            <form class="row" action="/facturar" method="post">
                @csrf
                <div class="col-12 mb-5 inline">
                    {{ $cliente->name }}
                    <input type="hidden" name="cliente_id" value="{{ $cliente->id }}" />
                    <button class="btn btn-link ml-5">Crear factura</button>
                </div>
            </form>
        @endif
    @endforeach
    @if(!count($clientes))
        <div class="text-danger">
            No hay compras pendientes por facturar.
        </div>
    @endif

    @if(count($facturas))
        <div class="text-primary h2">Facturas creadas</div>
        <table class="table">
            <thead>
                <tr>
                    <th>
                        Nro Factura
                    </th>
                    <th>
                        Fecha
                    </th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($facturas as $factura)
                    <tr>
                        <td>
                            {{ $factura->id }}
                        </td>
                        <td>
                            {{ $factura->created_at->format('d-m-yy') }}
                        </td>
                        <td>
                            <form action="/ver_detalle" method="get">
                                <input type="hidden" name="factura_id" value="{{ $factura->id }}">
                                <button type="submit" class="btn btn-link">
                                    ver detalle
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif
    <a href="/dashboard" class="btn btn-primary my-3">Volver</a>
@endsection